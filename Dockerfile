FROM openjdk:8u111-jdk-alpine
EXPOSE 9003
VOLUME /tmp
ADD /target/*.jar *.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/*.jar"]
